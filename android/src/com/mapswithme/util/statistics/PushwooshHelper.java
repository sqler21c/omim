package com.mapswithme.util.statistics;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class PushwooshHelper
{
  private static final Logger LOGGER = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.MISC);

  public void sendTags(@NonNull String tag, @Nullable String[] params)
  {
  }

  public static native void nativeProcessFirstLaunch();
  public static native void nativeSendEditorAddObjectTag();
  public static native void nativeSendEditorEditObjectTag();
}
