package com.mapswithme.maps.purchase;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapswithme.maps.base.Detachable;
import com.mapswithme.util.statistics.Statistics;

import java.util.Collections;
import java.util.List;

class BookmarkPurchaseCallback
    extends StatefulPurchaseCallback<BookmarkPaymentState, BookmarkPaymentFragment>
    implements PurchaseCallback, Detachable<BookmarkPaymentFragment>, CoreStartTransactionObserver
{
  @Nullable
  private Boolean mPendingValidationResult;
  @NonNull
  private final String mServerId;

  BookmarkPurchaseCallback(@NonNull String serverId)
  {
    mServerId = serverId;
  }

  @Override
  public void onStartTransaction(boolean success, @NonNull String serverId, @NonNull String
      vendorId)
  {
    if (!success)
    {
      activateStateSafely(BookmarkPaymentState.TRANSACTION_FAILURE);
      return;
    }

    activateStateSafely(BookmarkPaymentState.TRANSACTION_STARTED);
  }

  @Override
  public void onValidationFinish(boolean success)
  {
    if (getUiObject() == null)
      mPendingValidationResult = success;
    else
      getUiObject().handleValidationResult(success);

    activateStateSafely(BookmarkPaymentState.VALIDATION_FINISH);
  }

  @Override
  void onAttach(@NonNull BookmarkPaymentFragment uiObject)
  {
    if (mPendingValidationResult != null)
    {
      uiObject.handleValidationResult(mPendingValidationResult);
      mPendingValidationResult = null;
    }
  }
}
